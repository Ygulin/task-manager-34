package ru.tsc.gulin.tm.api.service;

import ru.tsc.gulin.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {
}
